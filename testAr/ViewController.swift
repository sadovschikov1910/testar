//
//  ViewController.swift
//  testAr
//
//  Created by Андрей Садовщиков on 19.04.2023.
//

import UIKit
import GLTFSceneKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {
    
    
    @IBOutlet weak var loadLabel: UILabel!
    private func downloadImageTask(){
        
        //1. Get The URL Of The Image
        guard let url = URL(string: LinkTarget) else { return }
        
        //2. Create The Download Session
        let downloadSession = URLSession(configuration: URLSession.shared.configuration, delegate: self, delegateQueue: nil)
        
        //3. Create The Download Task & Run It
        let downloadTask = downloadSession.downloadTask(with: url)
        downloadTask.resume()
    }
    
    @IBOutlet var sceneView: ARSCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadImageTask()
        sceneView.delegate = self
        sceneView.showsStatistics = true
    }
    
    private func getDocumentsDirectory() -> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    private func loadedImagesFromDirectoryContents() -> Set<ARReferenceImage>?{
        
        var index = 0
        var customReferenceSet = Set<ARReferenceImage>()
        let documentsDirectory = getDocumentsDirectory()
        
        do {
            
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsDirectory, includingPropertiesForKeys: nil, options: [])
            let filteredContents = directoryContents.filter{ $0.pathExtension == "png" }
            filteredContents.forEach { (url) in
                
                do{
                    //1. Create A Data Object From Our URL
                    let imageData = try Data(contentsOf: url)
                    guard let image = UIImage(data: imageData) else { return }
                    
                    //2. Convert The UIImage To A CGImage
                    guard let cgImage = image.cgImage else { return }
                    
                    //4. Create A Custom AR Reference Image With A Unique Name
                    let customARReferenceImage = ARReferenceImage(cgImage, orientation: CGImagePropertyOrientation.up, physicalWidth: 8)
                    customARReferenceImage.name = "MyCustomARImage\(index)"
                    
                    //4. Insert The Reference Image Into Our Set
                    customReferenceSet.insert(customARReferenceImage)
                    
                    index += 1
                    
                }catch{
                }
            }
            
        } catch {
        }
        
        return customReferenceSet
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let configuration = ARImageTrackingConfiguration()
        
        guard let trackedImages = loadedImagesFromDirectoryContents() else {
            print("No images available")
            return
        }
        
        configuration.trackingImages = trackedImages
        configuration.maximumNumberOfTrackedImages = 1
        
        // Run the view's session
        self.sceneView.session.run(configuration)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
    
    
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        DispatchQueue.main.async {
            self.loadLabel.isHidden = false;
        }
        
        let node = SCNNode()
        
        if let imageAnchor = anchor as? ARImageAnchor {
            let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)
            
            plane.firstMaterial?.diffuse.contents = UIColor(white: 1, alpha: 1)
            
            let planeNode = SCNNode(geometry: plane)
            planeNode.eulerAngles.z = .pi * 2
            
            let myURL = NSURL(string: LinkModel)
            DispatchQueue.main.async {
                var shipScene: SCNScene
                
                let sceneSource =  GLTFSceneSource(url: myURL! as URL)
                shipScene = try! sceneSource.scene()
                
                let shipNode = shipScene.rootNode
                shipNode.position = SCNVector3Zero
                shipNode.position.z = 0.15
                
                planeNode.addChildNode(shipNode)
                
                node.addChildNode(planeNode)
                self.loadLabel.isHidden = true;
            }
        }
        return node
    }
    
}

extension  ViewController: URLSessionDownloadDelegate
{
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        let fileURL = getDocumentsDirectory().appendingPathComponent("image.png")
        do {
            try FileManager.default.copyItem(at: location, to: fileURL)
            
            print("Successfuly Saved File \(fileURL)")
            
        } catch {
            
            print("Error Saving: \(error)")
        }
        
    }
}
